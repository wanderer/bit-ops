#include <stdio.h>

void zeroth_bit_test();
void do_arbitrary_stuff();

unsigned char PTAD;

void do_bit_ops() {
	PTAD = 0x10;
	printf("PTAD = %#x\n* OR\n", PTAD);
	PTAD |= 0x01;
	printf("PTAD |= 0x01 --> %#x\n", PTAD);

	printf("----------\n* AND\n");
	printf("PTAD = %#x\n", PTAD=0x1f);
	PTAD &= 0xfe;
	printf("PTAD &= 0xfe --> %#x\n", PTAD);

	printf("----------\n* NOT\n");
	printf("PTAD = %#x\n", PTAD=0x10);
	PTAD = ~PTAD;
	printf("PTAD = ~PTAD --> %#x\n", PTAD);

	printf("----------\n* XOR\n");
	printf("PTAD = %#x\n", PTAD=0x10);
	PTAD ^= 0xfe;
	printf("PTAD ^= 0xfe --> %#x\n", PTAD);

	zeroth_bit_test();
	do_arbitrary_stuff();
}

void zeroth_bit_test() {
	PTAD = 0x10;
	printf("----------\n* 0th bit test\n");
	for (PTAD = 0x00; PTAD < 0x0a; PTAD++){
		printf("PTAD = %u (", PTAD);
		for (int i = 0; i < 8; i++) {
			printf("%d", !!((PTAD << i) & 0x80));
		}
		printf(")\n");
		if ( PTAD & 0x01 ) {
			printf("0th bit is 1\n");
		} else {
			printf("0th bit is 0\n");
		}
	}
}

void gimme_bin_int(char byte) {
	for (int i = 0; i < 8; i++) {
		printf("%d", !!((byte << i) & 0x80));
	}
	printf(")\n");
}

void do_arbitrary_stuff() {
	unsigned char PTCD;
	/* set 1st bit to 1 */
	printf("----------\n* set 1st bit to 1\n");
	printf("PTCD = %#x\n", PTCD=0x00);
	PTCD |= 0x01;
	printf("PTCD |= 0x01 --> %#x (", PTCD);
	gimme_bin_int(PTCD);

	printf("----------\n* set 2nd bit to 1\n");
	printf("PTCD = %u (", PTCD=0b101);
	gimme_bin_int(PTCD);
	PTCD |= 0b111;
	printf("PTCD |= 7 --> %#x (", PTCD);
	gimme_bin_int(PTCD);

	printf("----------\n* set {6,7}th bit to 1\n");
	printf("PTCD = %u (", PTCD=0x3a);
	gimme_bin_int(PTCD);
	PTCD |= 0xc0;
	printf("PTCD |= 1110 0000 --> %#x (", PTCD);
	gimme_bin_int(PTCD);

	printf("----------\n* set 0th bit to 0\n");
	printf("PTCD = %u (", PTCD=0x5d);
	gimme_bin_int(PTCD);
	PTCD |= 0x50;
	printf("PTCD |= 0x50 --> %#x (", PTCD);
	gimme_bin_int(PTCD);

	printf("----------\n* set bit 1 and 2 to 0\n");
	printf("PTCD = %#x (", PTCD=0xc2);
	gimme_bin_int(PTCD);
	PTCD |= 0x03;
	printf("PTCD |= 0x00 --> %#x (", PTCD);
	gimme_bin_int(PTCD);

	char mask = 0x18;
	printf("----------\n* shift \"mask\" bits 2 positions to the left\n");
	printf("mask = %#x (", mask);
	gimme_bin_int(mask);
	mask <<= 2;
	printf("mask <<= 2 --> %#x (", mask);
	gimme_bin_int(mask);

	unsigned char PTBD = 0xbc;
	printf("----------\n* check if PTBD bit 0 is 1\n");
	printf("PTBD = %#x (", PTBD);
	gimme_bin_int(PTBD);
	if ( PTBD & 0x01 ) {
		printf("PTBD bit 0 is 1\n");
	} else {
		printf("PTBD bit 0 is 0\n");
	}
}


int main() {
	printf("*** bit-ops pls ***\n");
	do_bit_ops();
	return 0;
}
